FROM rabbitmq:3.8-management-alpine

MAINTAINER "Mael FOSSO"

COPY rabbitmq.config /etc/rabbitmq
COPY rabbit_mfhouse.json /etc/rabbitmq

CMD ["rabbitmq-server"]

